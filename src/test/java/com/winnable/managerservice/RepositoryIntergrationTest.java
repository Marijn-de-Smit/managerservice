package com.winnable.managerservice;

import domain.Account;
import enums.RoleType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import repository.AccountRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
public class RepositoryIntergrationTest {

    @MockBean
    private AccountRepository accountRepository;

    Optional<Account> accountOptional;
    
    List<Account> accounts = new ArrayList<Account>();

    @Before
    public void SetUp() {
        Mockito.when(accountRepository.save(any(Account.class))).thenReturn(new Account(12, "updated", RoleType.MANAGER));
        Mockito.when(accountRepository.findAll()).thenReturn(accounts);
        Mockito.when(accountRepository.findById(12)).thenReturn(accountOptional);
    }

    @Test
    public void createFuel() {
        // when
        Account item = new Account(12, "updated", RoleType.MANAGER);
        Account inserted = accountRepository.save(item);
        accounts.add(item);

        // then
        assertThat(inserted.getId()).isEqualTo(item.getId());
    }

    @Test
    public void getAllFuel() {
        // when
        List<Account> found = accountRepository.findAll();

        // then
        assertThat(found.size()).isEqualTo(1);
    }

    @Test
    public void getFuelById() {
        // when
        Optional<Account> found = accountRepository.findById(12);

        // then
        assertThat(found.get().getId()).isEqualTo(12);
    }

    @Test
    public void updateFuel() {
        // when
        Optional<Account> beforeUpdate = accountRepository.findById(12);
        Account update = beforeUpdate.get();
        update.setName("updated");
        accountRepository.save(update);
        Optional<Account> afterUpdate = accountRepository.findById(12);

        // then
        assertThat(beforeUpdate.get().getName()).isNotEqualTo(afterUpdate.get().getName());
    }

    @Test
    public void DeleteFuel() {
        // when
        List<Account> beforeDelete = accountRepository.findAll();
        Account item = new Account(12, "updated", RoleType.MANAGER);
        accountRepository.delete(item);
        List<Account> afterDelete = accountRepository.findAll();

        // then
        assertThat(beforeDelete.size()).isNotEqualTo(afterDelete.size());
    }

}
