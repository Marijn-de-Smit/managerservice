package service;

import domain.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.AccountRepository;

import java.util.List;

@Service
public class AccountService implements IAccountService {

    @Autowired
    private AccountRepository accountRepository;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Account createUser(Account account) {
        Account createdAccount = null;
        if (isValid(account)) {
            createdAccount = accountRepository.save(account);
        }
        return createdAccount;
    }

    @Override
    public Account updateUser(Account account) {
        Account updatedAccount = null;
        if (isValid(account)) {
            updatedAccount = accountRepository.save(account);
        }
        return updatedAccount;
    }

    @Override
    public boolean deleteUser(Account account) {
        boolean isRemoved = false;
        if (isValid(account)) {
            try {
                accountRepository.delete(account);
                isRemoved = true;
            } catch (Exception ex) {
                logger.error(ex.getMessage());
            }
        }
        return isRemoved;
    }

    @Override
    public Account getUser(int id) {
        Account account = null;
        if (id > 0) {
            account = accountRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        }
        return account;
    }

    @Override
    public List<Account> getAllUsers() {
        return accountRepository.findAll();
    }

    private boolean isValid(Account account) {
        return account.getId() > 0 && account.getRoleType() != null && !account.getName().equals("") && account.getName() != null ? true : false;
    }

}
