package service;
import domain.Account;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IAccountService {

    Account createUser(Account account);

    Account updateUser(Account account);

    boolean deleteUser(Account account);

    Account getUser(int id);

    List<Account> getAllUsers();

}
