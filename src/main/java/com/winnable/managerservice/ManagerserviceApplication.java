package com.winnable.managerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableDiscoveryClient
@EntityScan("domain")
@EnableJpaRepositories(basePackages = "repository")
@SpringBootApplication
public class ManagerserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ManagerserviceApplication.class, args);
    }

}
