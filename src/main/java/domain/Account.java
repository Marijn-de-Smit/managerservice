package domain;

import dto.createDto.CreateAccountDTO;
import dto.updateDto.UpdateAccountDTO;
import enums.RoleType;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleType roleType;

    public Account() {

    }

    public Account(CreateAccountDTO createAccountDTO) {
        this.name = createAccountDTO.getName();
        this.roleType = Enum.valueOf(RoleType.class, createAccountDTO.getRoleType());
    }

    public Account(UpdateAccountDTO updateAccountDTO) {
        this.id = updateAccountDTO.getId();
        this.name = updateAccountDTO.getName();
        this.roleType = Enum.valueOf(RoleType.class, updateAccountDTO.getRoleType());
    }

    public Account(int id) {
        this.id = id;
    }

    public Account(String name, RoleType roleType) {
        this.name = name;
        this.roleType = roleType;
    }

    public Account(int id, String name, RoleType roleType) {
        this.id = id;
        this.name = name;
        this.roleType = roleType;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public RoleType getRoleType() {
        return roleType;
    }
    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }
}
