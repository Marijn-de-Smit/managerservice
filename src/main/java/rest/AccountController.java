package rest;

import com.google.gson.Gson;
import domain.Account;
import dto.createDto.CreateAccountDTO;
import dto.readDto.ReadAccountDTO;
import dto.updateDto.UpdateAccountDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import service.IAccountService;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@RestController
public class AccountController implements IAccountController {

    @Autowired
    private IAccountService userService;
    private Gson gson = new Gson();

    @Override
    public Response createUser(String createUserDTO) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        CreateAccountDTO userDTO = gson.fromJson(createUserDTO, CreateAccountDTO.class);
        Account account = new Account(userDTO);

        Account createdAccount = userService.createUser(account);

        if (createdAccount != null) {
            ReadAccountDTO readAccountDTO = new ReadAccountDTO(createdAccount);
            response.status(Response.Status.OK).entity(readAccountDTO);
        }
        return response.build();
    }

    @Override
    public Response updateUser(String updateUserDTO) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        UpdateAccountDTO userDTO = gson.fromJson(updateUserDTO, UpdateAccountDTO.class);
        Account account = new Account(userDTO);

        Account updatedAccount = userService.updateUser(account);

        if (updatedAccount != null) {
            ReadAccountDTO readAccountDTO = new ReadAccountDTO(updatedAccount);
            response.status(Response.Status.OK).entity(readAccountDTO);
        }

        return response.build();
    }

    @Override
    public Response deleteUser(int id) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        Account account = new Account(id);

        boolean isRemoved = userService.deleteUser(account);
        response.status(Response.Status.OK).entity(isRemoved);

        return response.build();
    }

    @Override
    public Response getUser(int id) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        Account account = userService.getUser(id);

        if (account != null) {
            ReadAccountDTO readAccountDTO = new ReadAccountDTO(account);
            response.status(Response.Status.OK).entity(readAccountDTO);
        }
        return response.build();
    }

    @Override
    public Response getAllUsers() {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        List<Account> accountList = userService.getAllUsers();

        if (!accountList.isEmpty()) {
            List<ReadAccountDTO> readAccountDTOList = new ArrayList<>();
            for (Account accountItem : accountList) {
                ReadAccountDTO readAccountDTO = new ReadAccountDTO(accountItem);
                readAccountDTOList.add(readAccountDTO);
            }
            response.status(Response.Status.OK).entity(readAccountDTOList);
        }
        return response.build();
    }
}
