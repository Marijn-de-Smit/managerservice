package rest;

import org.springframework.web.bind.annotation.*;
import javax.ws.rs.core.Response;

@RequestMapping("/user")
public interface IAccountController {

    @PostMapping("/create")
    Response createUser(@RequestBody String createUserDTO);

    @PutMapping("/update")
    Response updateUser(@RequestBody String updateUserDTO);

    @DeleteMapping("/delete/{id}")
    Response deleteUser(@PathVariable int id);

    @GetMapping("/get/{id}")
    Response getUser(@RequestParam int id);

    @GetMapping("/all")
    Response getAllUsers();
}
