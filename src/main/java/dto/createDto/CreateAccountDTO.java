package dto.createDto;

public class CreateAccountDTO {

    private String name;
    private String roleType;

    public CreateAccountDTO(String name, String roleType) {
        this.name = name;
        this.roleType = roleType;
    }

    public String getName() {
        return name;
    }
    public String getRoleType() {
        return roleType;
    }
}
