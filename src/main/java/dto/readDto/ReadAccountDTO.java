package dto.readDto;

import domain.Account;
import enums.RoleType;

public class ReadAccountDTO {

    private int id;
    private String name;
    private RoleType roleType;

    public ReadAccountDTO(Account account) {
        this.id = account.getId();
        this.name = account.getName();
        this.roleType = account.getRoleType();
    }

    public ReadAccountDTO(int id, String name, RoleType roleType) {
        this.id = id;
        this.name = name;
        this.roleType = roleType;
    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public RoleType getRoleType() {
        return roleType;
    }
}
