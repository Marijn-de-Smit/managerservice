package dto.updateDto;

public class UpdateAccountDTO {

    private int id;
    private String name;
    private String roleType;

    public UpdateAccountDTO(int id, String name, String roleType) {
        this.id = id;
        this.name = name;
        this.roleType = roleType;
    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getRoleType() {
        return roleType;
    }
}
