package dto.deleteDto;

public class DeleteAccountDTO {

    private int id;

    public DeleteAccountDTO(int id) {
        this.id = id;
    }

    public int getId() { return id; }
}
